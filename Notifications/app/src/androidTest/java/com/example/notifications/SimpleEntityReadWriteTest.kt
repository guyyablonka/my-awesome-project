package com.example.notifications

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.notifications.roomDB.database.RemindersDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import java.io.IOException
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class SimpleEntityReadWriteTest {
    @Inject
    lateinit var db: RemindersDatabase

    @Before
    fun clearDb() {
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }
}
