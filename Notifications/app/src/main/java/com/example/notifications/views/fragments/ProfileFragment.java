package com.example.notifications.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.notifications.R;
import com.example.notifications.activitiesFunctions.FragmentToolbar;
import com.example.notifications.activitiesFunctions.MessageViewer;
import com.example.notifications.navigation.Navigate;
import com.example.notifications.repositories.UpdateUserResult;
import com.example.notifications.viewModel.MainActivityViewModel;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import kotlin.Unit;

public class ProfileFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MainActivityViewModel mainViewModel;
    private EditText editUsername;
    private Navigate navigate;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AndroidSupportInjection.inject(this);

        if (getActivity() != null) {
            initViews(view);
            initNavigator();
            setHasOptionsMenu(true);
            setProfileToolbar();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_save_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.findItem(R.id.add_menu_button).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.save_menu_button && !getInputText().trim().isEmpty()) {
            setUserNameIfExist(getInputText());
        } else {
            ((MessageViewer) getActivity()).viewMessage(
                    getView(),
                    getString(R.string.username_taken_msg)
            );
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews(View view) {
        editUsername = view.findViewById(R.id.username_edit);
        mainViewModel = new ViewModelProvider(this, viewModelFactory)
                .get(MainActivityViewModel.class);
        mainViewModel.getConnectedUser().observe(getActivity(), user -> {
                    if (user != null) {
                        editUsername.setText(user.getUsername());
                    }
                }
        );
        mainViewModel.usernameTakenListener(this::userNameUpdateResult);
    }

    private Unit userNameUpdateResult(UpdateUserResult isSucceed) {
        if (isSucceed.getResult()) {
            navigate.returnBack();
        } else {
            ((MessageViewer) getActivity()).viewMessage(
                    getView(),
                    getActivity().getString(R.string.username_taken_msg)
            );
        }
        return Unit.INSTANCE;
    }

    private void initNavigator() {
        navigate = new Navigate(
                getActivity(),
                getContext(),
                getActivity().getSupportFragmentManager()
        );
    }

    private void setProfileToolbar() {
        ((FragmentToolbar) getActivity()).setFragmentToolBar(
                getString(R.string.Profile),
                true
        );
    }

    private void setUserNameIfExist(String newName) {
        mainViewModel.updateUser(newName);
    }

    private String getInputText() {
        return editUsername.getText().toString();
    }
}