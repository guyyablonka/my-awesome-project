package com.example.notifications.activitiesFunctions;

import android.view.View;

public interface MessageViewer {
    void viewMessage(View view, String message);
}
