package com.example.notifications.recyclerView

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notifications.R
import com.example.notifications.roomDB.models.Alert
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class AlertViewHolder(itemView: View, private val onAlertClickListener:
OnAlertClickListener)
    : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private var alert: Alert? = null
    private val title: TextView = itemView.findViewById(R.id.notification_title)
    private val description: TextView = itemView.findViewById(R.id.notification_description)
    private val hourTime: TextView = itemView.findViewById(R.id.notification_hour)
    private val dateTime: TextView = itemView.findViewById(R.id.notification_date)
    private val dayOfWeek: TextView = itemView.findViewById(R.id.notification_day)

    init {
        itemView.setOnClickListener(this)
    }

    fun bindView(alert: Alert) {
        this.alert = alert
        title.text = alert.title()
        description.text = alert.description()
        hourTime.text = getHour(alert.timeToAlert())
        dateTime.text = formattedDate(alert.timeToAlert())
        dayOfWeek.text = getDayOfWeek(alert.timeToAlert())
    }

    private fun getDayOfWeek(date: Date): String? {
        val formatter: DateFormat = SimpleDateFormat(
                itemView.resources.getString(R.string.formatted_day),
                Locale.ENGLISH
        )
        return formatter.format(date)
    }

    private fun formattedDate(date: Date): String? {
        val dateFormat = SimpleDateFormat(
                itemView.resources.getString(R.string.formatted_date),
                Locale.ENGLISH
        )
        return dateFormat.format(date)
    }

    private fun getHour(date: Date): String? {
        val hourFormat = SimpleDateFormat(
                itemView.resources.getString(R.string.formatted_hour),
                Locale.ENGLISH
        )
        return hourFormat.format(date)
    }

    override fun onClick(v: View) {
        onAlertClickListener.onAlertClick(alert)
    }
}