package com.example.notifications.repositories;

import android.content.Context;

import androidx.preference.PreferenceManager;

import com.example.notifications.R;

public class CurrentUserRepository {
    private static CurrentUserRepository instance;

    private CurrentUserRepository() {
    }

    public static CurrentUserRepository getInstance() {
        if (instance == null) {
            instance = new CurrentUserRepository();
        }

        return instance;
    }

    public String connectedUserId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(context.getString(R.string.userId), context.getString(R.string.zero));
    }

    public void changeConnectedUserId(Context context, String id) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString(context.getString(R.string.userId), id)
                .apply();
    }
}
