package com.example.notifications.notificationsHandler

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import androidx.core.app.NotificationCompat
import com.example.notifications.R
import com.example.notifications.views.activities.MainActivity

class NotificationHelper(base: Context?) : ContextWrapper(base) {
    private var mManager: NotificationManager? = null

    fun getManager(): NotificationManager? {
        if (mManager == null) {
            mManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        }
        return mManager
    }

    fun getChannelNotification(title: String?, description: String?, id: Int): Notification {
        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(
                this,
                id,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )
        return NotificationCompat.Builder(applicationContext, "channelID").run {
            setContentTitle(title)
            setContentText(description)
            setSmallIcon(R.drawable.logo)
            setContentIntent(resultPendingIntent)
            setAutoCancel(true)
            build()
        }
    }
}
