package com.example.notifications.roomDB.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.notifications.roomDB.daos.AlertDao
import com.example.notifications.roomDB.daos.UserDao
import com.example.notifications.roomDB.models.Alert
import com.example.notifications.roomDB.models.User

@Database(entities = [User::class, Alert::class], exportSchema = false, version = 4)
@TypeConverters(Converters::class)
abstract class RemindersDatabase : RoomDatabase() {
    abstract fun roomUserDao(): UserDao
    abstract fun roomAlertDao(): AlertDao

    companion object {
        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE TABLE    alert_new (alert_id TEXT NOT NULL, user_owner_id TEXT,
                                    title TEXT, description TEXT, time_to_alert INTEGER, 
                                    PRIMARY KEY(alert_id))""")
                database.execSQL("""
                    INSERT INTO alert_new (alert_id, user_owner_id,
                                title, description, time_to_alert) 
                    SELECT  alert_id, userOwnerId, title, description, time_to_alert 
                    FROM    alert""")
                database.execSQL("DROP TABLE alert")
                database.execSQL("ALTER TABLE alert_new RENAME TO alert")
            }
        }

        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {}
        }

        val MIGRATION_3_4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE TABLE    user_new (user_id TEXT NOT NULL, username TEXT,
                                    password TEXT, PRIMARY KEY(user_id))""")
                database.execSQL("""
                    INSERT INTO user_new (user_id, username, password) 
                    SELECT  user_id, username, password
                    FROM    user""")
                database.execSQL("DROP TABLE user")
                database.execSQL("ALTER TABLE user_new RENAME TO user")
            }
        }
    }
}