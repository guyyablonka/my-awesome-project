package com.example.notifications.activitiesFunctions

import com.example.notifications.roomDB.models.Alert

interface Callable {
    fun prepareFragment(alert: Alert)
    fun prepareToolbarMenu()
}