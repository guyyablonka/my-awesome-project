package com.example.notifications.di.components

import android.app.Application
import com.example.notifications.di.modules.ActivityModule
import com.example.notifications.di.modules.FragmentModule
import com.example.notifications.di.modules.RoomModule
import com.example.notifications.di.modules.ViewModelModule
import com.example.notifications.views.NotificationApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Component(modules = [
    RoomModule::class,
    ViewModelModule::class,
    ActivityModule::class,
    AndroidInjectionModule::class
])
@Singleton
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(notificationApplication: NotificationApplication)
}