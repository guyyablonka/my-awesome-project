package com.example.notifications.di.modules

import android.app.Application
import androidx.room.Room
import com.example.notifications.repositories.AlertRoomRepository
import com.example.notifications.repositories.UserRoomRepository
import com.example.notifications.roomDB.daos.AlertDao
import com.example.notifications.roomDB.daos.UserDao
import com.example.notifications.roomDB.database.RemindersDatabase
import com.example.notifications.roomDB.database.RemindersDatabase.Companion.MIGRATION_1_2
import com.example.notifications.roomDB.database.RemindersDatabase.Companion.MIGRATION_2_3
import com.example.notifications.roomDB.database.RemindersDatabase.Companion.MIGRATION_3_4
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RoomModule {
    @Singleton
    @Provides
    fun providesRoomDatabase(application: Application): RemindersDatabase {
        return Room.databaseBuilder(
                application,
                RemindersDatabase::class.java,
                "reminders.db")
                .addMigrations(MIGRATION_1_2)
                .addMigrations(MIGRATION_2_3)
                .addMigrations(MIGRATION_3_4)
                .build()
    }

    @Singleton
    @Provides
    fun providesUserDao(remindersDatabase: RemindersDatabase): UserDao {
        return remindersDatabase.roomUserDao()
    }

    @Singleton
    @Provides
    fun providesAlertDao(remindersDatabase: RemindersDatabase): AlertDao {
        return remindersDatabase.roomAlertDao()
    }

    @Singleton
    @Provides
    fun providesUserRepository(userDao: UserDao): UserRoomRepository {
        return UserRoomRepository(userDao)
    }

    @Singleton
    @Provides
    fun providesAlertRepository(alertDao: AlertDao): AlertRoomRepository {
        return AlertRoomRepository(alertDao)
    }
}