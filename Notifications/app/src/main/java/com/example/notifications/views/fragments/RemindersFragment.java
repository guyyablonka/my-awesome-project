package com.example.notifications.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notifications.R;
import com.example.notifications.activitiesFunctions.FragmentToolbar;
import com.example.notifications.navigation.Navigate;
import com.example.notifications.recyclerView.AlertListAdapter;
import com.example.notifications.roomDB.models.Alert;
import com.example.notifications.viewModel.MainActivityViewModel;
import com.example.notifications.viewModel.SocketService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class RemindersFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MainActivityViewModel mainViewModel;
    private Navigate navigate;
    private AlertListAdapter alertListAdapter;
    private RecyclerView alertsRecycleView;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_reminders, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        AndroidSupportInjection.inject(this);

        if (getActivity() != null) {
            initViewModel();
            initNavigator();
            setRemindersToolbar();
            setUpViews(view);
            initRecyclerView();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.profile) {
            navigate.openProfile();
        } else if (item.getItemId() == R.id.logout) {
            mainViewModel.getAlertsOfUser().observe(this, alerts ->
                    mainViewModel.cancelListOfNotifications(alerts));
            mainViewModel.logoutUser();
            stopSocketService();
            navigate.moveToLoginActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViewModel() {
        mainViewModel = new ViewModelProvider(this, viewModelFactory)
                .get(MainActivityViewModel.class);
    }

    private void initNavigator() {
        navigate = new Navigate(
                getActivity(),
                requireContext(),
                getActivity().getSupportFragmentManager()
        );
    }

    private void stopSocketService() {
        getActivity().getBaseContext().stopService(
                new Intent(getActivity().getBaseContext(), SocketService.class)
        );
    }

    private void setRemindersToolbar() {
        mainViewModel.getConnectedUser().observe(getViewLifecycleOwner(), user ->
                ((FragmentToolbar) getActivity()).setFragmentToolBar(
                        getString(R.string.hello_username, user.getUsername()),
                        false)
        );
    }

    private void addAlert() {
        navigate.openDetailsAddAlert();
    }

    private void onAlertClick(Alert alert) {
        navigate.openDetailsEditAlert(alert.getId());
    }

    private void initRecyclerView() {
        alertsRecycleView.addItemDecoration(
                new DividerItemDecoration(
                        alertsRecycleView.getContext(),
                        DividerItemDecoration.VERTICAL
                )
        );

        alertListAdapter = new AlertListAdapter(this::onAlertClick);
        mainViewModel.getAlertsOfUser().observe(
                requireActivity(),
                alerts -> alertListAdapter.submitAlerts(alerts)
        );

        alertsRecycleView.setLayoutManager(
                new LinearLayoutManager(getContext())
        );

        new ItemTouchHelper(setOnSwipeAlert()).attachToRecyclerView(alertsRecycleView);
        alertsRecycleView.setAdapter(alertListAdapter);
    }

    private ItemTouchHelper.SimpleCallback setOnSwipeAlert() {
        return new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder,
                                 int direction) {
                Alert alertToDelete = alertListAdapter.getAlertAt(
                        viewHolder.getAdapterPosition()
                );

                mainViewModel.deleteAlert(alertToDelete);
            }
        };
    }

    private void setUpViews(View view) {
        alertsRecycleView = view.findViewById(R.id.alertList_recycleView);
        FloatingActionButton addAlertButton = view.findViewById(R.id.add_reminder);
        addAlertButton.setOnClickListener(v -> addAlert());
    }
}