package com.example.notifications.views.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.notifications.R;
import com.example.notifications.activitiesFunctions.FragmentToolbar;
import com.example.notifications.activitiesFunctions.MessageViewer;
import com.example.notifications.navigation.Navigate;
import com.example.notifications.roomDB.models.Alert;
import com.example.notifications.viewModel.MainActivityViewModel;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class DetailsFragment extends Fragment {
    private static final String ALERT_ID = "alert_id";
    private final Calendar calendar = Calendar.getInstance();
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private Alert alert = null;
    private MainActivityViewModel mainViewModel;
    private Navigate navigate;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private Button dateButton;
    private TimePicker timePicker;
    private TextView dateViewText;
    private EditText reminderNameEditText;
    private EditText descriptionEditText;
    private Date pickedDate;

    public static DetailsFragment newInstance() {
        return new DetailsFragment();
    }

    public static DetailsFragment newInstance(String alertId) {
        DetailsFragment instance = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ALERT_ID, alertId);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        if (getActivity() != null) {
            initViewModel();
            setArgumentsByAlert();
            initNavigator();
            initViews(view);
            setDateButtonListener();
            prepareFragmentByArgument();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_save_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        Bundle bundle = this.getArguments();
        if (isArgsPassed(bundle)) {
            menu.findItem(R.id.save_menu_button).setVisible(true);
            menu.findItem(R.id.add_menu_button).setVisible(false);
        } else {
            menu.findItem(R.id.save_menu_button).setVisible(false);
            menu.findItem(R.id.add_menu_button).setVisible(true);
        }
    }

    private void prepareFragmentByArgument() {
        Bundle bundle = this.getArguments();
        if (isArgsPassed(bundle)) {
            setArgumentsByAlert();
        } else {
            setTodayDate();
            setDetailsToolbar(getString(R.string.add_reminder));
        }
    }

    private void setArgumentsByAlert() {
        Bundle bundle = this.getArguments();
        mainViewModel.getAlertById(bundle.getString(ALERT_ID)).observe(
                getViewLifecycleOwner(),
                alert -> {
                    this.alert = alert;
                    setArgumentsInViews();
                    setDetailsToolbar(getString(R.string.edit_reminder));
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Alert editedAlert = selectedAlert();
        if (isInputsFilled() && isFutureTime()) {
            if (item.getItemId() == R.id.save_menu_button) {
                Objects.requireNonNull(alert).setAlertDetails(editedAlert);
                mainViewModel.updateAlert(alert);
                ((MessageViewer) getActivity()).viewMessage(requireView(),
                        getString(R.string.reminder_edited_msg));
            } else if (item.getItemId() == R.id.add_menu_button) {
                mainViewModel.addAlert(editedAlert);
                ((MessageViewer) getActivity()).viewMessage(requireView(),
                        getString(R.string.reminder_added_msg));
            }

            navigate.returnBack();
        } else {
            ((MessageViewer) getActivity()).viewMessage(requireView(),
                    getString(R.string.field_required_msg));
        }

        return super.onOptionsItemSelected(item);
    }

    private Boolean isArgsPassed(Bundle bundle) {
        return bundle != null;
    }

    private void initViewModel() {
        mainViewModel = new ViewModelProvider(this, viewModelFactory)
                .get(MainActivityViewModel.class);
    }

    private void initNavigator() {
        navigate = new Navigate(
                getActivity(),
                getContext(),
                getActivity().getSupportFragmentManager()
        );
    }

    private void setDetailsToolbar(String header) {
        ((FragmentToolbar) getActivity()).setFragmentToolBar(header, true);
    }

    private void setDateButtonListener() {
        dateButton.setOnClickListener(v -> {
            DatePickerDialog dialog = new DatePickerDialog(
                    getContext(),
                    dateSetListener,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            dialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
            dialog.show();
        });

        onChooseDate();
    }

    private void onChooseDate() {
        dateSetListener = (view, year, month, dayOfMonth) -> {
            Calendar cal = Calendar.getInstance();
            DateFormat formatter = new SimpleDateFormat(
                    getString(R.string.formatted_date_and_day),
                    Locale.ENGLISH
            );
            cal.set(year, month, dayOfMonth);
            dateViewText.setText(formatter.format(cal.getTime()));
            pickedDate = cal.getTime();
        };
    }

    private Alert selectedAlert() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(pickedDate);
        cal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
        cal.set(Calendar.MINUTE, timePicker.getCurrentMinute());
        cal.set(Calendar.SECOND, 0);
        pickedDate = cal.getTime();
        return new Alert(
                mainViewModel.currentUserId(),
                reminderNameEditText.getText().toString(),
                descriptionEditText.getText().toString(),
                pickedDate
        );
    }

    private boolean isFutureTime() {
        return pickedDate.after(calendar.getTime())
                && (timePicker.getCurrentHour() > calendar.get(Calendar.HOUR_OF_DAY)
                || (timePicker.getCurrentHour() == calendar.get(Calendar.HOUR_OF_DAY)
                && timePicker.getCurrentMinute() >= calendar.get(Calendar.MINUTE)));
    }

    private void initViews(View view) {
        dateViewText = view.findViewById(R.id.edit_text_date);
        dateButton = view.findViewById(R.id.date_button);
        reminderNameEditText = view.findViewById(R.id.reminder_name_edit_text);
        descriptionEditText = view.findViewById(R.id.description_edit_text);
        timePicker = view.findViewById(R.id.time_picker);
        timePicker.setIs24HourView(true);
    }

    private void setTodayDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(
                getString(R.string.formatted_date_and_day),
                Locale.ENGLISH
        );
        dateViewText.setText(dateFormatter.format(calendar.getTime()));
        pickedDate = calendar.getTime();
    }

    private void setArgumentsInViews() {
        pickedDate = alert.timeToAlert();
        dateViewText.setText(formattedDateAndDay(alert.timeToAlert()));
        reminderNameEditText.setText(alert.title());
        descriptionEditText.setText(alert.description());
        timePicker.setCurrentHour(getHourOfTime(alert.timeToAlert()));
        timePicker.setCurrentMinute(getMinuteOfTime(alert.timeToAlert()));
    }

    public int getHourOfTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public int getMinuteOfTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MINUTE);
    }

    public String formattedDateAndDay(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                getString(R.string.formatted_date_and_day),
                Locale.ENGLISH
        );
        return dateFormat.format(date);
    }

    private boolean isInputsFilled() {
        return !reminderNameEditText.getText().toString().trim().isEmpty()
                && !descriptionEditText.getText().toString().trim().isEmpty();
    }
}