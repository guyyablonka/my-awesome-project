package com.example.notifications.roomDB.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.notifications.roomDB.models.Alert

@Dao
interface AlertDao {
    @Query("SELECT * FROM alert WHERE alert_id = :id")
    fun getAlert(id: String): LiveData<Alert>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAlert(alert: Alert)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAlerts(alerts: List<Alert>)

    @Update
    suspend fun editAlert(alert: Alert)

    @Delete
    suspend fun deleteAlert(alert: Alert)

    @Query("SELECT * FROM alert WHERE user_owner_id = :id")
    fun getAlertsByUserId(id : String): LiveData<List<Alert>>

    @Query("SELECT * FROM alert WHERE alert_id NOT IN (:ids)")
    suspend fun getAlertsToDelete(ids: List<String>): List<Alert>

    @Delete
    suspend fun deleteAlertsByIds(ids: List<Alert>)

    @Transaction
    suspend fun updateAllAlerts(alerts: List<Alert>): List<Alert> {
        val alertToDelete = getAlertsToDelete(alerts.map { it.id })
        deleteAlertsByIds(alertToDelete)
        addAlerts(alerts)
        return alertToDelete
    }
}