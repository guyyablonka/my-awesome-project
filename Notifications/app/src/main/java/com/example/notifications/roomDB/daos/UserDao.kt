package com.example.notifications.roomDB.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.notifications.roomDB.models.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user WHERE user.user_id = :id")
    fun getUserById(id: String): LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: User)

    @Update
    suspend fun updateUser(user: User)
}