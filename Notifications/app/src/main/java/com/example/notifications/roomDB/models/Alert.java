package com.example.notifications.roomDB.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@Entity(tableName = "alert")
public class Alert implements Parcelable {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "alert_id")
    private String id;

    @ColumnInfo(name = "user_owner_id")
    private final String userId;

    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "time_to_alert")
    private Date timeToAlert;

    public Alert(String userId, String title, String description, Date timeToAlert) {
        this.id = UUID.randomUUID().toString();
        this.userId = userId;
        this.title = title;
        this.description = description;
        this.timeToAlert = timeToAlert;
    }

    protected Alert(Parcel in) {
        id = in.readString();
        userId = in.readString();
        title = in.readString();
        description = in.readString();
    }

    public static final Creator<Alert> CREATOR = new Creator<Alert>() {
        @Override
        public Alert createFromParcel(Parcel in) {
            return new Alert(in);
        }

        @Override
        public Alert[] newArray(int size) {
            return new Alert[size];
        }
    };

    public void setAlertDetails(Alert alert) {
        this.title = alert.title();
        this.description = alert.description();
        this.timeToAlert = alert.timeToAlert();
    }

    public String getUserId() {
        return userId;
    }

    public @NotNull String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    public String title() {
        return title;
    }

    public String description() {
        return description;
    }

    public Date timeToAlert() {
        return timeToAlert;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(userId);
        dest.writeString(title);
        dest.writeString(description);
    }
}