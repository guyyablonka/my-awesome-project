package com.example.notifications.recyclerView

import com.example.notifications.roomDB.models.Alert

interface OnAlertClickListener {
    fun onAlertClick(alert: Alert?)
}