package com.example.notifications.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.notifications.R
import com.example.notifications.notificationsHandler.NotificationHandler
import com.example.notifications.repositories.*
import com.example.notifications.roomDB.models.Alert
import com.example.notifications.roomDB.models.User
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
        var userRepository: UserRoomRepository,
        var alertRepository: AlertRoomRepository,
        app: Application
) : AndroidViewModel(app) {
    private val currentUserRepository = CurrentUserRepository.getInstance()

    fun logoutUser() {
        currentUserRepository.changeConnectedUserId(
                getApplication(),
                getApplication<Application>().getString(R.string.zero)
        )
    }

    fun getAlertById(id: String): LiveData<Alert> {
        return alertRepository.getAlertById(id)
    }

    fun currentUserId(): String {
        return currentUserRepository.connectedUserId(getApplication())
    }

    fun getConnectedUser(): LiveData<User> {
        return userRepository.getUserById(
                currentUserRepository.connectedUserId(getApplication())
        )
    }

    fun getAlertsOfUser(): LiveData<List<Alert>> {
        return alertRepository.getAlertsOfUser(currentUserId())
    }

    // socket
    private fun syncAlerts(newAlerts: List<Alert>) = viewModelScope.launch {
        val alertToCancel = alertRepository.updateAllAlerts(newAlerts)
        cancelListOfNotifications(alertToCancel)
        notifyListOfNotifications(newAlerts)
    }

    fun cancelListOfNotifications(alerts: List<Alert>) {
        alerts.forEach { alert ->
            NotificationHandler.cancelNotification(getApplication(), alert)
        }
    }

    private fun notifyListOfNotifications(alerts: List<Alert>) {
        alerts.forEach { alert ->
            if (alert.timeToAlert().after(Date())) {
                NotificationHandler.scheduleNotification(getApplication(), alert)
            }
        }
    }

    fun syncUserWithAlerts() {
        viewModelScope.launch {
            SocketRepository.syncUserWithAlerts(currentUserId())
        }
    }

    fun usernameTakenListener(updateUserResult: (isSucceed: UpdateUserResult) -> Unit) {
        SocketRepository.turnOnUsernameUpdateResult(updateUserResult)
    }

    fun fetchUserAlertsOnStartListener() {
        SocketRepository.turnOnFetchUserAlertsIfExist(::syncAlerts)
    }

    fun addAlert(alert: Alert) {
        NotificationHandler.scheduleNotification(getApplication(), alert)
        viewModelScope.launch {
            SocketRepository.createAlert(alert)
            alertRepository.addAlert(alert)
        }
    }

    fun updateAlert(alert: Alert) {
        NotificationHandler.scheduleNotification(getApplication(), alert)
        viewModelScope.launch {
            SocketRepository.updateAlert(alert)
            alertRepository.updateAlert(alert)
        }
    }

    fun deleteAlert(alert: Alert) {
        NotificationHandler.cancelNotification(getApplication(), alert)
        viewModelScope.launch {
            SocketRepository.deleteAlert(alert)
            alertRepository.deleteAlert(alert)
        }
    }

    fun updateUser(name: String) {
        viewModelScope.launch {
            SocketRepository.updateUser(name, currentUserId())
        }
    }
}