package com.example.notifications.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.notifications.repositories.CurrentUserRepository
import com.example.notifications.repositories.SocketRepository
import com.example.notifications.repositories.UserRoomRepository
import com.example.notifications.roomDB.models.User
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginActivityViewModel @Inject constructor(
        var userRepository: UserRoomRepository,
        app: Application
) : AndroidViewModel(app) {
    private val currentUserRepository = CurrentUserRepository.getInstance()

    fun getUser(): LiveData<User> {
        return userRepository.getUserById(loggedInUserId())
    }

    fun upsertUser(user: User) = viewModelScope.launch {
        userRepository.insertUser(user)
    }

    fun loggedInUserId(): String {
        return currentUserRepository.connectedUserId(getApplication())
    }

    fun changeConnectedUserId(id: String) {
        currentUserRepository.changeConnectedUserId(getApplication(), id)
    }

    // socket
    fun loginUser(user: User) {
        viewModelScope.launch {
            SocketRepository.loginUser(user)
        }
    }

    fun turnOnListeners(loginConfirmed: (User) -> Unit, passwordNotMatchMessage: () -> Unit) {
        SocketRepository.turnOnLoginListeners(loginConfirmed, passwordNotMatchMessage)
    }

    fun turnOffListeners() {
        SocketRepository.turnOffLoginListeners()
    }
}
