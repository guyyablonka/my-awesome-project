package com.example.notifications.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.notifications.viewModel.LoginActivityViewModel
import com.example.notifications.viewModel.MainActivityViewModel
import com.example.notifications.viewModel.ViewModelFactory
import com.example.notifications.viewModel.ViewModelKeys
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory):
            ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKeys.ViewModelKey(MainActivityViewModel::class)
    protected abstract fun mainViewModel(mainViewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKeys.ViewModelKey(LoginActivityViewModel::class)
    protected abstract fun loginViewModel(loginViewModel: LoginActivityViewModel): ViewModel
}