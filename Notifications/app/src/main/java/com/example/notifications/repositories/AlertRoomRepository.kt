package com.example.notifications.repositories

import androidx.lifecycle.LiveData
import com.example.notifications.roomDB.daos.AlertDao
import com.example.notifications.roomDB.models.Alert
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AlertRoomRepository @Inject constructor(var alertDao: AlertDao) {
    fun getAlertById(id: String): LiveData<Alert> {
        return alertDao.getAlert(id)
    }

    suspend fun updateAlert(alert: Alert) {
        alertDao.editAlert(alert)
    }

    suspend fun deleteAlert(alert: Alert) {
        alertDao.deleteAlert(alert)
    }

    suspend fun addAlert(alert: Alert) {
        alertDao.addAlert(alert)
    }

    fun getAlertsOfUser(id: String): LiveData<List<Alert>> {
        return alertDao.getAlertsByUserId(id)
    }

    suspend fun updateAllAlerts(alertsToDelete: List<Alert>): List<Alert> {
        return alertDao.updateAllAlerts(alertsToDelete)
    }
}