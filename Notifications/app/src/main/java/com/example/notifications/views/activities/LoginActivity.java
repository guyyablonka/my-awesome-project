package com.example.notifications.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import com.example.notifications.R;
import com.example.notifications.activitiesFunctions.MessageViewer;
import com.example.notifications.navigation.Navigate;
import com.example.notifications.roomDB.models.User;
import com.example.notifications.viewModel.LoginActivityViewModel;
import com.example.notifications.viewModel.SocketService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import kotlin.Unit;

public class LoginActivity extends AppCompatActivity implements MessageViewer {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private LoginActivityViewModel loginViewModel;
    private FloatingActionButton continueButton;
    private EditText usernameInput;
    private EditText passwordInput;
    private Navigate navigate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        AndroidInjection.inject(this);
        setLoginViewModel();
        navigate = new Navigate(this, this, getSupportFragmentManager());
        skipLoginActivity();
        observeUser();
        setViewsById();
        onFillLoginFields();
    }

    private Unit loginConfirmed(User user) {
        loginViewModel.upsertUser(user);
        loginViewModel.changeConnectedUserId(user.getId());
        return Unit.INSTANCE;
    }

    private Unit passwordNotMatchMessage() {
        viewMessage(getCurrentFocus(), getString(R.string.password_not_match));
        return Unit.INSTANCE;
    }

    private void startSocketService() {
        Intent intent = new Intent(getBaseContext(), SocketService.class);
        getBaseContext().startService(intent);
    }

    private void observeUser() {
        loginViewModel.getUser().observe(this, connectedUser -> {
            if (isUserLoggedIn()) {
                connectUser();
            }
        });
    }

    private void skipLoginActivity() {
        if (isUserLoggedIn()) {
            startSocketService();
            connectUser();
        }
    }

    private boolean isUserLoggedIn() {
        return !loginViewModel.loggedInUserId().equals(getString(R.string.zero));
    }

    private void onFillLoginFields() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s,
                                      int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable string) {
                if (!passwordInput().trim().isEmpty() && !usernameInput().trim().isEmpty()) {
                    switchButtonState(true, R.color.green_add_notification);
                } else {
                    switchButtonState(false, R.color.grey);
                }
            }
        };

        usernameInput.addTextChangedListener(textWatcher);
        passwordInput.addTextChangedListener(textWatcher);
    }

    private void switchButtonState(boolean enable, int color) {
        continueButton.setEnabled(enable);
        continueButton.setBackgroundTintList(ContextCompat
                .getColorStateList(this, color));
    }

    private void loginRequest() {
        startSocketService();
        loginViewModel.turnOnListeners(this::loginConfirmed, this::passwordNotMatchMessage);
        loginViewModel.loginUser(new User(usernameInput(), passwordInput()));
    }

    private void connectUser() {
        navigate.moveToMainActivity();
        loginViewModel.turnOffListeners();
    }

    private void setLoginViewModel() {
        loginViewModel = new ViewModelProvider(this, viewModelFactory)
                .get(LoginActivityViewModel.class);
    }

    private void setViewsById() {
        usernameInput = findViewById(R.id.username);
        passwordInput = findViewById(R.id.password);
        continueButton = findViewById(R.id.continue_button);
        continueButton.setOnClickListener(v -> loginRequest());
    }

    private String usernameInput() {
        return usernameInput.getText().toString();
    }

    private String passwordInput() {
        return passwordInput.getText().toString();
    }

    @Override
    public void viewMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }
}