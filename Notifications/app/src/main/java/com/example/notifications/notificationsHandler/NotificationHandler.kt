package com.example.notifications.notificationsHandler

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.notifications.R
import com.example.notifications.roomDB.models.Alert
import com.example.notifications.viewModel.SocketService

class NotificationHandler {
    companion object {
        fun scheduleNotification(context: Context, alert: Alert) {
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, AlertReceiver::class.java)
            intent.putExtras(getArgumentBundle(context, alert))

            alarmManager.setExact(
                    AlarmManager.RTC_WAKEUP,
                    alert.timeToAlert().time,
                    getPendingIntent(context, alert, intent)
            )
        }

        private fun getArgumentBundle(context: Context, alert: Alert): Bundle {
            return Bundle().apply {
                putInt(context.getString(R.string.id), alert.id.hashCode())
                putString(context.getString(R.string.title), alert.title())
                putString(context.getString(R.string.description_value), alert.description())
            }
        }

        private fun getPendingIntent(context: Context, alert: Alert, intent: Intent): PendingIntent {
            return PendingIntent.getBroadcast(
                    context,
                    alert.id.hashCode(),
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            )
        }

        fun cancelNotification(context: Context, alert: Alert) {
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, AlertReceiver::class.java)
            alarmManager.cancel(PendingIntent.getBroadcast(
                    context,
                    alert.id.hashCode(),
                    intent,
                    PendingIntent.FLAG_CANCEL_CURRENT
            ))
        }
    }
}