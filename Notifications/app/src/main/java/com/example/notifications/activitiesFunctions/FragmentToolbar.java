package com.example.notifications.activitiesFunctions;

public interface FragmentToolbar {
    void setFragmentToolBar(String resTitle, boolean withBackArrow);
}
