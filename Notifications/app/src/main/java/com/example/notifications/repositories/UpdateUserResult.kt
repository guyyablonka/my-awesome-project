package com.example.notifications.repositories

data class UpdateUserResult(var result: Boolean, var message: String)
