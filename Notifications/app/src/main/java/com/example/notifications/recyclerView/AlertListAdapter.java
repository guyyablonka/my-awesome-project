package com.example.notifications.recyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notifications.R;
import com.example.notifications.roomDB.models.Alert;

import java.util.ArrayList;
import java.util.List;

public class AlertListAdapter
        extends RecyclerView.Adapter<AlertViewHolder> {
    private final OnAlertClickListener onAlertClickListener;
    private List<Alert> alerts = new ArrayList<>();

    public AlertListAdapter(OnAlertClickListener onAlertClickListener) {
        this.onAlertClickListener = onAlertClickListener;
    }

    public Alert getAlertAt(int position) {
        return alerts.get(position);
    }

    public void submitAlerts(List<Alert> observableAlerts) {
        this.alerts = observableAlerts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AlertViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_row, parent, false);
        return new AlertViewHolder(itemView, onAlertClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AlertViewHolder holder, int position) {
        holder.bindView(alerts.get(position));
    }

    @Override
    public int getItemCount() {
        return alerts == null ? 0 : alerts.size();
    }
}