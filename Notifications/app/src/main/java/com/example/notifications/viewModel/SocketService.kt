package com.example.notifications.viewModel

import android.app.Notification
import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.example.notifications.R
import com.example.notifications.notificationsHandler.NotificationHandler
import com.example.notifications.notificationsHandler.NotificationHelper
import com.example.notifications.repositories.AlertRoomRepository
import com.example.notifications.repositories.SocketRepository
import com.example.notifications.repositories.UserRoomRepository
import com.example.notifications.roomDB.models.Alert
import com.example.notifications.roomDB.models.User
import dagger.android.AndroidInjection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class SocketService : Service(), CoroutineScope {
    @Inject
    lateinit var userRepository: UserRoomRepository
    @Inject
    lateinit var alertRepository: AlertRoomRepository
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    override fun onCreate() {
        AndroidInjection.inject(this)
    }

    override fun onBind(intent: Intent?): IBinder? = null


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        SocketRepository.connectServer()
        turnOnListeners()
        startForeground(startId, stickyNotification(startId))

        return START_STICKY
    }

    private fun turnOnListeners() {
        SocketRepository.turnOnServiceListeners(
                ::updateUser,
                ::createAlert,
                ::deleteAlert,
                ::updateAlert
        )
    }

    private fun stickyNotification(startId: Int): Notification {
        val notificationHelper = NotificationHelper(this)
        return notificationHelper.getChannelNotification(
                getString(R.string.service),
                getString(R.string.service_description),
                startId
        )
    }

    private fun createAlert(alert: Alert) {
        NotificationHandler.scheduleNotification(applicationContext, alert)
        launch { alertRepository.addAlert(alert) }
    }

    private fun deleteAlert(alert: Alert) {
        NotificationHandler.cancelNotification(applicationContext, alert)
        launch { alertRepository.deleteAlert(alert) }
    }

    private fun updateAlert(alert: Alert) {
        NotificationHandler.scheduleNotification(applicationContext, alert)
        launch { alertRepository.updateAlert(alert) }
    }

    private fun updateUser(user: User) {
        launch { userRepository.updateUser(user) }
    }

    override fun onDestroy() {
        SocketRepository.disconnectServer()
        SocketRepository.turnOffMainListeners()
    }
}