package com.example.notifications.di.modules

import com.example.notifications.viewModel.SocketService
import com.example.notifications.views.activities.LoginActivity
import com.example.notifications.views.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun contributeSocketService(): SocketService
}