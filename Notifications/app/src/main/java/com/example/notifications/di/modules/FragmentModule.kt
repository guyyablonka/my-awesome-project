package com.example.notifications.di.modules

import com.example.notifications.views.fragments.DetailsFragment
import com.example.notifications.views.fragments.ProfileFragment
import com.example.notifications.views.fragments.RemindersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun contributeRemindersFragment(): RemindersFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailsFragment(): DetailsFragment
}