package com.example.notifications.notificationsHandler

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import dagger.android.AndroidInjection

class AlertReceiver : BroadcastReceiver() {
    private var id: Int = 0
    private var title: String? = null
    private var description: String? = null

    override fun onReceive(context: Context?, intent: Intent) {
        val notificationHelper = NotificationHelper(context)
        setParameters(intent)
        notificationHelper.getManager()?.notify(
                id,
                notificationHelper.getChannelNotification(title, description, id)
        )
        //context?.startService(Intent(context, SocketService::class.java))
    }

    private fun setParameters(intent: Intent) {
        id = intent.getIntExtra("id", 0)
        title = intent.getStringExtra("title")
        description = intent.getStringExtra("description")
    }
}
