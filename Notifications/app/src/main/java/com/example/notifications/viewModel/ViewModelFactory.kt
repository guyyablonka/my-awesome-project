package com.example.notifications.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

@Singleton
class ViewModelFactory @Inject constructor(
        private val viewModels: MutableMap<Class<out ViewModel>,
                Provider<ViewModel>>
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (viewModels[modelClass]?.get()) {
            is MainActivityViewModel,
            is LoginActivityViewModel ->
                viewModels[modelClass]?.get() as T
            else -> throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}