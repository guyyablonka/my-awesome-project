package com.example.notifications.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.notifications.R;
import com.example.notifications.roomDB.models.Alert;
import com.example.notifications.views.activities.LoginActivity;
import com.example.notifications.views.activities.MainActivity;
import com.example.notifications.views.fragments.DetailsFragment;
import com.example.notifications.views.fragments.ProfileFragment;
import com.example.notifications.views.fragments.RemindersFragment;

public class Navigate {
    private final FragmentManager fragmentManager;
    private final Activity currentActivity;
    private final Context context;

    public Navigate(Activity currentActivity, Context context, FragmentManager fragmentManager) {
        this.currentActivity = currentActivity;
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    public void toReminder() {
        replaceFragment(new RemindersFragment());
    }

    public void openProfile() {
        addFragment(new ProfileFragment());
    }

    public void openDetailsAddAlert() {
        addFragment(DetailsFragment.newInstance());
    }

    public void openDetailsEditAlert(String alertId) {
        addFragment(DetailsFragment.newInstance(alertId));
    }

    private void replaceFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    private void addFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void returnBack() {
        fragmentManager.popBackStack();
    }

    private void moveToNewActivity(Class<?> newActivity) {
        currentActivity.startActivity(new Intent(context, newActivity));
        currentActivity.finish();
    }

    public void moveToLoginActivity() {
        moveToNewActivity(LoginActivity.class);
    }

    public void moveToMainActivity() {
        moveToNewActivity(MainActivity.class);
    }
}
