package com.example.notifications.views.activities;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import com.example.notifications.R;
import com.example.notifications.activitiesFunctions.FragmentToolbar;
import com.example.notifications.activitiesFunctions.MessageViewer;
import com.example.notifications.navigation.Navigate;
import com.example.notifications.viewModel.MainActivityViewModel;
import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class MainActivity extends AppCompatActivity
        implements MessageViewer, FragmentToolbar, HasAndroidInjector {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    DispatchingAndroidInjector<Object> dispatchingAndroidInjector;
    private Navigate navigate;
    private Toolbar toolbar;
    private MainActivityViewModel mainActivityViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        AndroidInjection.inject(this);
        setMainViewModel();
        setSocketListenersAndEmitters();
        initNavigator();
        initToolBar();
        navigate.toReminder();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount()
                == getResources().getInteger(R.integer.zero_size)) {
            finish();
        }

        navigate.returnBack();
    }

    private void setSocketListenersAndEmitters() {
        mainActivityViewModel.fetchUserAlertsOnStartListener();
        mainActivityViewModel.syncUserWithAlerts();
    }

    private void initNavigator() {
        navigate = new Navigate(this, this, getSupportFragmentManager());
    }

    private void setMainViewModel() {
        mainActivityViewModel = new ViewModelProvider(this, viewModelFactory)
                .get(MainActivityViewModel.class);
    }

    private void initToolBar() {
        toolbar = findViewById(R.id.notification_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void setFragmentToolBar(String resTitle, boolean withBackArrow) {
        toolbar.setTitle(resTitle);
        if (withBackArrow) {
            toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
            toolbar.setNavigationOnClickListener(v -> navigate.returnBack());
        } else {
            toolbar.setNavigationIcon(null);
        }
    }

    @Override
    public void viewMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingAndroidInjector;
    }
}