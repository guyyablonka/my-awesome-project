package com.example.notifications.repositories

import androidx.lifecycle.LiveData
import com.example.notifications.roomDB.daos.UserDao
import com.example.notifications.roomDB.models.User
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRoomRepository @Inject constructor(var userDao: UserDao) {
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    fun getUserById(id: String): LiveData<User> {
        return userDao.getUserById(id)
    }

    suspend fun insertUser(user: User) = withContext(ioDispatcher) {
        userDao.insertUser(user)
    }

    suspend fun updateUser(user: User) = withContext(ioDispatcher) {
        userDao.updateUser(user)
    }
}
