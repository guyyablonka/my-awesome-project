package com.example.notifications.repositories

import com.example.notifications.roomDB.models.Alert
import com.example.notifications.roomDB.models.User
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException
import java.util.*

object SocketRepository {
    private const val SERVER_URL = "http://192.168.189.2:3000"
    private const val FETCH_ALERTS_ON_START = "fetchUserAlertsIfExist"
    private const val CREATED_ALERT = "createdAlert"
    private const val UPDATED_ALERT = "updatedAlert"
    private const val DELETED_ALERT = "deletedAlert"
    private const val UPDATED_USER = "updatedUser"
    private const val USERNAME_UPDATE_RESULT = "usernameChangeResult"
    private const val PASSWORD_NOT_MATCH = "passwordNotMatch"
    private const val USER_CONFIRMED = "userConfirmed"
    private const val CREATE_ALERT = "createAlert"
    private const val UPDATE_ALERT = "updateAlert"
    private const val DELETE_ALERT = "deleteAlert"
    private const val UPDATE_USER = "updateUser"
    private const val SYNC_APP = "syncApp"
    private const val LOGIN_USER = "loginUser"

    private val socket: Socket
    private val gson = Gson()

    init {
        try {
            socket = IO.socket(SERVER_URL)

        } catch (e: URISyntaxException) {
            throw e
        }
    }

    fun connectServer() {
        socket.connect()
    }

    fun disconnectServer() {
        socket.disconnect()
    }

    fun turnOnLoginListeners(loginConfirmed: (User) -> Unit,
                             passwordNotMatchMessage: () -> Unit) {
        socket.apply {
            on(PASSWORD_NOT_MATCH) {
                passwordNotMatchMessage()
            }
            on(USER_CONFIRMED) {
                loginConfirmed(getUserFromJson(it.first() as String?))
            }
        }
    }

    fun turnOnFetchUserAlertsIfExist(fetchUserAlertsIfExist: (List<Alert>) -> Unit) {
        socket.apply {
            on(FETCH_ALERTS_ON_START) {
                fetchUserAlertsIfExist(getAlertListFromJson(it.first() as String?))
            }
        }
    }

    fun turnOnUsernameUpdateResult(onUsernameTaken: (isSucceed: UpdateUserResult) -> Unit) {
        socket.apply{
            on(USERNAME_UPDATE_RESULT) {
                onUsernameTaken(gson.fromJson(it.first() as String, UpdateUserResult::class.java))
            }
        }
    }

    fun turnOnServiceListeners(
            getUpdatedUser: (User) -> Unit,
            getCreatedAlert: (Alert) -> Unit,
            getDeletedAlert: (Alert) -> Unit,
            getUpdatedAlert: (Alert) -> Unit) {
        socket.apply {
            on(UPDATED_USER) {
                getUpdatedUser(getUserFromJson(it.first() as String?))
            }
            on(CREATED_ALERT) {
                getCreatedAlert(getAlertFromJson(it.first() as String))
            }
            on(DELETED_ALERT) {
                getDeletedAlert(getAlertFromJson(it.first() as String))
            }
            on(UPDATED_ALERT) {
                getUpdatedAlert(getAlertFromJson(it.first() as String))
            }
        }
    }

    private fun getAlertFromJson(data: String?): Alert {
        return gson.fromJson(data, Alert::class.java)
    }

    private fun getAlertListFromJson(data: String?): List<Alert> {
        return gson.fromJson(data, object : TypeToken<List<Alert?>?>() {}.type)
    }

    private fun getUserFromJson(data: String?): User {
        return gson.fromJson(data, User::class.java)
    }

    fun turnOffLoginListeners() {
        socket.apply {
            off(USER_CONFIRMED)
            off(PASSWORD_NOT_MATCH)
        }
    }

    fun turnOffMainListeners() {
        socket.apply {
            off(FETCH_ALERTS_ON_START)
            off(CREATED_ALERT)
            off(DELETED_ALERT)
            off(UPDATED_ALERT)
            off(UPDATED_USER)
            off(USERNAME_UPDATE_RESULT)
        }
    }

    suspend fun createAlert(alert: Alert) {
        socket.emit(CREATE_ALERT, gson.toJson(alert))
    }

    suspend fun updateAlert(alert: Alert) {
        socket.emit(UPDATE_ALERT, gson.toJson(alert))
    }

    suspend fun deleteAlert(alert: Alert) {
        socket.emit(DELETE_ALERT, gson.toJson(alert))
    }

    suspend fun updateUser(name: String, userId: String) {
        socket.emit(UPDATE_USER, name, userId)
    }

    suspend fun syncUserWithAlerts(userId: String) {
        socket.emit(SYNC_APP, userId)
    }

    suspend fun loginUser(user: User) {
        socket.emit(LOGIN_USER, gson.toJson(user))
    }
}